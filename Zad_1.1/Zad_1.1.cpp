// Zad_1.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>  
#include <stdio.h>  
#include <stdlib.h> 
#include "iostream"
#include <omp.h>  

DWORD ThreadProc(LPVOID* theArg)
{
	//printf("Witam %d\n", theArg);
	return TRUE;
}

int main(int argc, char *argv[])
{
	DWORD dwThreadID, dwThreadParam = 1;
	HANDLE hThread;
	double start, stop, result;

	// Utworzenie 1 watku
	start = omp_get_wtime();
	hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadProc, &dwThreadParam, 0, &dwThreadID);
	WaitForSingleObject(hThread, INFINITE);
	CloseHandle(hThread);
	stop = omp_get_wtime();
	result = stop - start;

	std::cout << "Czas utworzenia 1 watku " << result << " sekund." << std::endl;
	// Utworzenie 100 watkow
	start = omp_get_wtime();
	for (int i = 0; i<100; i++){
		hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadProc, 0, 0, &dwThreadID);
		WaitForSingleObject(hThread, INFINITE);
		CloseHandle(hThread);
	}
	stop = omp_get_wtime();

	result = stop - start;
	std::cout << "Czas utworzenia 100 watkow " << result << " sekund." << std::endl;

	system("Pause");
	return 0;
}

